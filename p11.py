from sys import stdin

next(stdin)
results = []
for line in stdin:
    a, b, c = map(int, line.split())
    results.append(sum(map(int, str(a * b + c))))
print(' '.join(map(str, results)))

from sys import stdin


def to_seconds(days, hours, minutes, seconds):
    return days * 86400 + hours * 3600 + minutes * 60 + seconds


def from_seconds(seconds):
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    return days, hours, minutes, seconds

next(stdin)
results = []
for line in stdin:
    d, h, m, s, d2, h2, m2, s2 = map(int, line.split())
    results.append(str(from_seconds(to_seconds(d2, h2, m2, s2) -
                                    to_seconds(d, h, m, s))))
print(' '.join(results).replace(',', ''))

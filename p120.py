from sys import stdin

next(stdin)
nums = list(map(int, next(stdin).split()))
results = []
for a in range(len(nums) - 1):
    max_index, max_value = max(enumerate(nums), key=lambda b: b[1])
    results.append(max_index)
    if max_value == nums[-1]:
        nums.pop()
    else:
        nums[max_index] = nums.pop()
print(' '.join(map(str, results)))

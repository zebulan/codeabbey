from itertools import permutations
from sys import stdin

with open('words.txt', 'r') as f:
    all_words = {word.rstrip() for word in f}

results = []
next(stdin)
for line in stdin:
    clean = line.rstrip()
    results.append(sum(''.join(word) in all_words
                       for word in set(permutations(clean, len(clean)))) - 1)
print(' '.join(map(str, results)))

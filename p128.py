from math import factorial
from sys import stdin


def combinations(n, k):
    return '{:.0f}'.format(factorial(n) / (factorial(k) * factorial(n - k)))

next(stdin)
print(' '.join(combinations(*map(int, line.split())) for line in stdin))

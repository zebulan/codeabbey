from sys import stdin

next(stdin)
print(' '.join(map(str, (sum(int(a) * i for i, a in enumerate(num, 1))
                         for num in next(stdin).split()))))

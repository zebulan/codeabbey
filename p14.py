from functools import reduce
from sys import stdin

print(reduce(lambda a, b: str(eval(a + b.strip())),
             (line for line in stdin), next(stdin).strip()))

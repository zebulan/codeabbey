from sys import stdin

minimum, *_, maximum = sorted(map(int, next(stdin).split()))
print('{} {}'.format(maximum, minimum))

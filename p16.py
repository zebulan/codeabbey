from sys import stdin

next(stdin)
results = []
for line in stdin:
    length = -1  # added 0 at the end of each line, useless!
    total = 0
    for num in map(int, line.split()):
        total += num
        length += 1
    results.append(str(round(total / length)))
print(' '.join(results))

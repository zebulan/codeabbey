from functools import reduce
from sys import stdin

next(stdin)
print(reduce(lambda a, b: (a + b) * 113 % 10000007,
             map(int, next(stdin).split()), 0))

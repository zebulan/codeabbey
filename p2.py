from sys import stdin

_, nums = stdin.readlines()
print(sum(map(int, nums.split())))

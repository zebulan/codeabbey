from sys import stdin

VOWELS = set('aeiouyAEIOUY')
next(stdin)
print(' '.join(str(sum(a in VOWELS for a in line)) for line in stdin))

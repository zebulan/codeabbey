from sys import stdin

words = next(stdin)
print(' '.join(str(index) for _, index in
               sorted((words[a:], a) for a in range(len(words)))))

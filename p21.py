from collections import Counter
from sys import stdin

next(stdin)
print(' '.join(str(v) for _, v in
               sorted(Counter(map(int, next(stdin).split())).items())))

from functools import reduce
from sys import stdin

*nums, _ = map(int, next(stdin).split())
swap_cnt = 0
for i in range(len(nums) - 1):
    if nums[i] > nums[i + 1]:
        nums[i], nums[i + 1] = nums[i + 1], nums[i]
        swap_cnt += 1
print('{} {}'.format(swap_cnt,
                     reduce(lambda a, b: (a + b) * 113 % 10000007, nums, 0)))

from sys import stdin

next(stdin)
results = []
for num in map(int, next(stdin).split()):
    cnt = 0
    tmp = num
    seen = set()
    while True:
        _, rem = map(int, divmod(tmp ** 2 / 100, 10000))
        cnt += 1
        if rem in seen:
            break
        tmp = rem
        seen.add(rem)
    results.append(cnt)
print(' '.join(map(str, results)))

from sys import stdin


def gcd_lcm(a, b):
    ab = a * b
    while not a == b:
        if a < b:
            b -= a
        else:
            a -= b
    return '({} {})'.format(a, int(ab / a))

next(stdin)
print(' '.join(gcd_lcm(*map(int, line.split())) for line in stdin))

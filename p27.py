from sys import stdin

next(stdin)
nums = list(map(int, next(stdin).split()))
length = len(nums) - 1
total_passes = 0
total_swaps = 0
while True:
    tmp = total_swaps
    for i in range(length):
        if nums[i] > nums[i + 1]:
            nums[i], nums[i + 1] = nums[i + 1], nums[i]
            total_swaps += 1
    total_passes += 1
    if tmp == total_swaps:
        break
print(total_passes, total_swaps)

from sys import stdin

next(stdin)
results = []
for line in stdin:
    weight, height = map(float, line.split())
    bmi = round(weight / height ** 2, 1)
    if bmi < 18.5:
        results.append('under')
    elif bmi < 25.0:
        results.append('normal')
    elif bmi < 30.0:
        results.append('over')
    else:
        results.append('obese')
print(' '.join(map(str, results)))

from sys import stdin

next(stdin)
nums = ((value, str(index)) for index, value in
        enumerate(map(int, next(stdin).split()), 1))
print(' '.join(index for _, index in sorted(nums)))

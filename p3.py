from sys import stdin

next(stdin)
print(' '.join(str(sum(map(int, line.split()))) for line in stdin))

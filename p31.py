from sys import stdin

next(stdin)
results = []
for line in stdin:
    k, s = line.split()
    k = int(k)
    results.append(s[k:] + s[:k])
print(' '.join(results))

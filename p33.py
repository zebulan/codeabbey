from sys import stdin

message = []
for num in map(int, next(stdin).split()):
    binary = '{:b}'.format(num).zfill(8)
    if not sum(a == '1' for a in binary) % 2:
        message.append(chr(int(binary[1:], 2)))
print(''.join(message))

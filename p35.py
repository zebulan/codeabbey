from sys import stdin


def years_until(money, required_money, interest_rate):
    interest_rate /= 100
    year = 0
    while money < required_money:
        money = float('{:.2f}'.format(money + (money * interest_rate)))
        year += 1
    return str(year)

next(stdin)
print(' '.join(years_until(*map(int, line.split())) for line in stdin))

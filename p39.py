from statistics import mean, stdev
from sys import stdin

results = []
next(stdin)
for line in stdin:
    stock_name, *prices = line.split()
    prices = list(map(int, prices))
    if stdev(prices) > mean(prices) / 100 * 4:
        results.append(stock_name)
print(' '.join(results))

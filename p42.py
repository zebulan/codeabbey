from itertools import combinations_with_replacement
from sys import stdin

CARD_SCORE = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7,
              '8': 8, '9': 9, 'T': 10, 'J': 10, 'Q': 10, 'K': 10}


def highest_score(cards):
    ace_cnt = 0
    total = 0
    for card in cards:
        try:
            total += CARD_SCORE[card]
        except KeyError:
            ace_cnt += 1
    if ace_cnt:
        current_max = 0
        for ace_combo in combinations_with_replacement('01', ace_cnt):
            score = total + sum(1 if a == '0' else 11 for a in ace_combo)
            if current_max < score < 22:
                current_max = score
        total = current_max
    return '{}'.format(total if 0 < total < 22 else 'Bust')

next(stdin)
print(' '.join(map(highest_score, map(str.split, stdin))))

from string import ascii_uppercase as AZ
from sys import stdin

_, k = map(int, next(stdin).split())
print(' '.join((''.join(AZ[(AZ.index(a) - k) % 26] if a.isalpha() else a
                        for a in line.rstrip())) for line in stdin))

from sys import stdin


def collatz(n):
    result = 0
    while n != 1:
        if n % 2 == 0:
            n /= 2
        else:
            n = n * 3 + 1
        result += 1
    return result

next(stdin)
print(' '.join(map(str, map(collatz, map(int, next(stdin).split())))))

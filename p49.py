from sys import stdin


def winner_is(games):
    draw = ('RR', 'PP', 'SS')
    p1_wins = ('RS', 'SP', 'PR')
    p1_score = 0
    p2_score = 0
    for game in games.split():
        if game in draw:
            continue
        elif game in p1_wins:
            p1_score += 1
        else:
            p2_score += 1
    return '1' if p1_score > p2_score else '2'

next(stdin)
print(' '.join(map(winner_is, stdin)))

from sys import stdin


def is_palindrome(string):
    string = ''.join(a for a in string.lower() if a.isalpha())
    return 'Y' if string == string[::-1] else 'N'

next(stdin)
print(' '.join(is_palindrome(line) for line in stdin))

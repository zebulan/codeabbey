from sys import stdin

next(stdin)
results = []
for line in stdin:
    a, b, c = map(int, line.split())
    result = (a ** 2 + b ** 2) ** 0.5
    if result == c:
        results.append('R')
    elif result > c:
        results.append('A')
    else:
        results.append('O')
print(' '.join(results))

from collections import Counter
from sys import stdin

print(' '.join(k for k, v in sorted(
    filter(lambda a: a[1] > 1, Counter(next(stdin).split()).items()))))

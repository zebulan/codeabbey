from sys import stdin

next(stdin)
nums = list(map(float, next(stdin).split()))
length = len(nums) - 1
results = []
for i, num in enumerate(nums):
    if 0 < i < length:
        results.append(round(sum(nums[i - 1:i + 2]) / 3, 10))
    else:
        results.append(num)
print(' '.join(map(str, results)))

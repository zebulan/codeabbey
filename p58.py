from sys import stdin

SUITS = ('Clubs', 'Spades', 'Diamonds', 'Hearts')
RANKS = ('2', '3', '4', '5', '6', '7', '8', '9', '10',
         'Jack', 'Queen', 'King', 'Ace')
OUTPUT = '{}-of-{}'.format


def rank_of_suit(card_num):
    suit_value, rank_value = divmod(card_num, 13)
    return OUTPUT(RANKS[rank_value], SUITS[suit_value])

next(stdin)
print(' '.join(map(rank_of_suit, map(int, next(stdin).split()))))

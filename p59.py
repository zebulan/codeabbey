from sys import stdin

original, _ = next(stdin).split()
orig_set = set(original)
results = []
for num in next(stdin).split():
    correct_position = 0
    incorrect_position = 0
    for a, b in zip(original, num):
        if a == b:
            correct_position += 1
        elif b in orig_set:
            incorrect_position += 1
    results.append('{}-{}'.format(correct_position, incorrect_position))
print(' '.join(results))

from functools import reduce
from sys import stdin

next(stdin)
print(' '.join(str(reduce(lambda a, b: round(a / b),
                          map(int, line.split()))) for line in stdin))

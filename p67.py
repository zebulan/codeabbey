from sys import stdin


def fibonacci(total):
    a, b = 0, 1
    cnt = 0
    fib_nums = []
    while cnt < total:
        fib_nums.append(a)
        a, b = b, a + b
        cnt += 1
    return fib_nums

fib_1000 = fibonacci(1000)  # first 1000 fibonacci numbers
next(stdin)
print(' '.join(str(fib_1000.index(int(line))) for line in stdin))

from sys import stdin


def fibonacci():
    a, b = 1, 1
    index = 1
    while True:
        yield index, a
        a, b = b, a + b
        index += 1

next(stdin)
print(' '.join(next(str(i) for i, f in fibonacci() if not f % num)
               for num in map(int, next(stdin).split())))

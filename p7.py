from sys import stdin

print(' '.join(str(round((int(a) - 32) / 1.8))
               for a in next(stdin).split()[1:]))

from itertools import groupby
from sys import stdin

combos = {(2,): 'pair', (3,): 'three', (4,): 'four', (5,): 'yacht',
          (2, 2): 'two-pairs', (2, 3): 'full-house',
          (1, 2, 3, 4, 5): 'small-straight', (2, 3, 4, 5, 6): 'big-straight'}

results = []
next(stdin)
for line in stdin:
    try:
        nums = tuple(sorted(map(int, line.split())))
        results.append(combos[nums])
    except KeyError:
        results.append(combos.get(tuple(sorted(filter(lambda a: a > 1,
            (len(list(g)) for _, g in groupby(nums))))), 'none'))
print(' '.join(results))

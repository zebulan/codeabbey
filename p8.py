from sys import stdin

next(stdin)
results = []
for line in stdin:
    a, b, n = map(int, line.split())
    result = a
    for c in range(1, n):
        result += a + (c * b)
    results.append(str(result))
print(' '.join(results))

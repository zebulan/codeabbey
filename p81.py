from sys import stdin

next(stdin)
MASK = int('0b' + '1' * 32, 2)
print(' '.join(str(bin(a & MASK).count('1'))
               for a in map(int, next(stdin).split())))

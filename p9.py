from sys import stdin

next(stdin)
results = []
for line in stdin:
    a, b, c = map(int, line.split())
    results.append('1' if a + b > c and a + c > b and b + c > a else '0')
print(' '.join(results))
